const path = require("path");

const Retrieval = require(path.join(__dirname, "..", "..", "src", "Retrieval.js"));
let quotes = require("./data/quotes"); 
const express = require("express");

let rt = new Retrieval(K=2, B=0.75);

rt.index(quotes);

const app = express();
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));
app.use(express.urlencoded()) 

app.get("/", (req, res) => {
	res.render("form", {
		data: quotes
   });
});

app.post("/", (req, res) => {
	res.render("results", {
		found: rt.search(req.body.query, 10),
		data: quotes
   });
});

app.listen(8080);
console.log("La aplicación BM25 está corriendo en el puerto 8080");