
const vocab = require("./lib/vocab.js");
const idf = require("./lib/idf.js");

module.exports = (function() {
  return function(tfMaps) {

          var uniqTerms = vocab(tfMaps.map(Object.keys, Object)
                               );

          var idfMap = new Object(); 

          uniqTerms.forEach(function(term) {
            idfMap[term] = idf(term, tfMaps);
          });

          return idfMap;
        };
})();