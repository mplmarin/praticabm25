
module.exports = (function() {

  return function(term, termFreqMaps) {
            var numDocs = termFreqMaps.length;
            var docsAppear = 0;
            for(let i in termFreqMaps) {

              docsAppear += ( termFreqMaps[i].hasOwnProperty(term) ? 1 : 0 );
            }

            return 1 + Math.log(numDocs / (1 + docsAppear));
         };
})();
