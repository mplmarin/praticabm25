
module.exports = (function() {
  
  Set.prototype.__arrUnion = function(arr) {
    arr.forEach(this.add, this);
  }

  return function(matr) {
          var unionOfVector = new Set();
          matr.forEach(unionOfVector.__arrUnion, unionOfVector); 
          
          return unionOfVector;
         };
})();
