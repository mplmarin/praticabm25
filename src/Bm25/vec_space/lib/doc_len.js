
var _ = require('lodash');

module.exports = (function() {

	return function(matr) {
		    	let docLengths = matr.map(row => row.length);

		    	let avDocLength = _.sum(docLengths) / matr.length;
		    	return docLengths.map(docLength => docLength / avDocLength);
		    };
})();
