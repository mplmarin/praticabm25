var termCountVectorizerModule = (function() {

  function tallyTermFreq(runningCountTable, curTerm) {
    if(curTerm in runningCountTable){
      runningCountTable[curTerm]++;
    }
    else{
      runningCountTable[curTerm] = 1;
    }
    return runningCountTable;
  }
  return function(corpus) {
          return corpus.map(function(document) {
            return document.reduce(tallyTermFreq, {});
          });
        };
})();

module.exports = termCountVectorizerModule;