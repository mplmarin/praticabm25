
const _ = require('lodash');
const math = require('mathjs');
const Bm25 = require('./Bm25/Bm25.js');
const query2vec = require('./query/query2vec.js');
const chunkFilterStem = require('./nlp/chunk_filter_stem.js');
const topIndices = require('./score_selection/top_indices.js');

module.exports = (function() {
	//CONSTRUCTOR
  	var Retrieval = function(K=1.6, B=0.75) {
		this.docArray = [];
		this.K = K;
		this.B = B;
		this.docIndex = {};
		this.termIndex = {};
  	};

     
  	//METODOS
  	Retrieval.prototype.index = function(docArray) {
		this.docArray = docArray;

		let bm25 = new Bm25(corpusMatr = this.docArray.map(chunkFilterStem),
								  K = this.K,
                          B = this.B
                         );

		this.docIndex = math.sparse(bm25.buildMatr(), 
											 'number'
											);

		this.termIndex = bm25.getTerms();
	};


  	Retrieval.prototype.search = function(query_, N=10) {

      try {
			var queryVector = query2vec(query_, 
												 this.termIndex
												);
		}
		catch(error) {
			return [];
		}
      
      let docScores = [].concat(...math.multiply(this.docIndex,
																 queryVector
																)
													.valueOf()
										 );


		let topNIndices = topIndices(docScores).slice(0, N);

		return topNIndices.map((idx) => this.docArray[idx])
	};


   return Retrieval;
})();