const chunker = require("./lib/chunk.js");
const removeStopwords = require("./lib/filter_stopwords.js");
const porter = require("./lib/stem_porter2.js");

module.exports = function(sentence) {
  var lower = sentence.toLowerCase();
  var tokens = chunker(lower);
  var noStopwords = removeStopwords(tokens);
  var stems = noStopwords.map(porter.stem, porter);
  return stems;
};
