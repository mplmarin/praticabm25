

module.exports = (function(){

	function keepOnlyLetters(sentence){
	  return sentence.replace(/[^a-z]/gi, ' ');
	};


	function getChunks(sentence){
	  var camelCaseSplit = sentence.replace(/([a-z](?=[A-Z]))/g, '$1 ');
	  return camelCaseSplit.match(/\S+/g) || [];
	};

	return function(sentence) {
            var onlyLetters = keepOnlyLetters(sentence);
            var tokens = getChunks(onlyLetters);
            return tokens;
           };

})();
