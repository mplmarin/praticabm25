//en este apartado se definen las palabras excluidas
var removeStopwords = function(arrWords) {
  return arrWords.filter(word => !removeStopwords.stopwords.has(word)); // take set difference.
}

removeStopwords.stopwords = new Set(['page',
// 'ca',
// "n't",
// "'s",
'a',
'ante',
'bajo',
'cabe',
'con',
'contra',
'de',
'desde',
'en',
'entre',
'hacia',
'hasta',
'para',
'por',
'segun',
'sin',
'so',
'sobre',
'tras']);

module.exports = removeStopwords;
