const _ = require('lodash');
const chunkFilterStem = require('../../nlp/chunk_filter_stem.js');
const initArray = require('../../util/init_array.js');
const termLookup = require('../../util/term_lookup.js');

module.exports = function(query_, termIndex_) {
    let terms = chunkFilterStem(query_);

    let indexPositions = termLookup(terms, termIndex_)
    if (indexPositions.length == 0) {
        throw new Error('Term not found in index.');
    }
    
    let weightsAtPositions = initArray(indexPositions.length,
                                       1);
    return _.zipObject(indexPositions, weightsAtPositions);
};