const _ = require('lodash');
const makeZeros = require('../util/make_zeros.js');
const locateKeywords = require('./lib/locate_keywords.js');
const setEntries = require('../util/set_entries.js');

module.exports = function(query_, termIndex_) {
	zeroVec = makeZeros(_.size(termIndex_));
	wordLocs = locateKeywords(query_, termIndex_);
	return setEntries(zeroVec,
							wordLocs
						  );
};