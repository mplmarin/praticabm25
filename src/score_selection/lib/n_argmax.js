
const _ = require('lodash');
const Heap = require('heap');

module.exports = function(arr, n=10) {

    return _.pick(_.invertBy(arr),
                  Heap.nlargest(arr, n)
                 );
};
