const _ = require('lodash');

module.exports = function(len_, value_=0) {
    return _.fill(Array(len_), value_);
};
