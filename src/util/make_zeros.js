const math = require('mathjs');

module.exports = function(len_) {
    return math.zeros(len_, 1, 'sparse');
};
