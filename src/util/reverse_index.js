const _ = require('lodash');

module.exports = function(arr_, transform_) {
    return _.mapValues(_.invertBy(arr_),
                       function(value) {
                          return transform_(value);
                       });
};
