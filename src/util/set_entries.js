const makeZeros = require('./make_zeros.js');

module.exports = function(vector, locatValueDict) {
    for(const [location, value] of Object.entries(locatValueDict)) {
        vector.set([parseInt(location, 10), 0], value);
    }
    return vector;
}